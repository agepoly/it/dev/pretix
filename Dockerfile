FROM pretix/standalone:2024.2

USER root
#RUN pip3 install --no-cache-dir pretix-manualseats

WORKDIR /pretix/src

ENV DJANGO_SETTINGS_MODULE=

RUN git clone https://gitlab.com/agepoly/it/dev/pretix-datatrans.git && \
  cd pretix-datatrans && \
  pip3 install . && \
  cd .. && chown -R pretixuser:pretixuser .

# RUN git clone https://github.com/Omarelheni/pretix-seating.git && \
#   cd pretix-seating && \
#   pip3 install . && \
#   cd ..

# RUN git clone https://github.com/moritzlerch/pretix-manualseats.git && \
#   cd pretix-manualseats && \
#   pip3 install . && \
#   cd .. && chown -R pretixuser:pretixuser .

ENV DJANGO_SETTINGS_MODULE=production_settings

USER pretixuser
#WORKDIR /pretix/src 

# ADD ./seating_plan /pretix/src/pretix/plugins/seating_plan
ADD ./seating_plan /plugins/seating_plan
RUN cp -r /plugins/seating_plan /pretix/src/pretix/plugins/.

ADD ./pretix_override/_base_settings.py /pretix/src/pretix/_base_settings.py
ADD ./pretix_override/MANIFEST.in /pretix/src/pretix/MANIFEST.in

RUN make production

# RUN python3 manage.py show_urls | grep seating
